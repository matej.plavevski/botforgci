'using strict'
var addic7edApi = require('addic7ed-api');
const winston = require('winston');
const Botkit = require('botkit')
const config = require('./config.js')
const fs = require('fs');
var subscene_scraper=require('subscene_scraper');
var request = require('request');
var guessit = require('guessit-wrapper');
//const OpenSubtitles = require('opensubtitles-api');

var path=process.cwd();

const tsFormat = () => (new Date()).toLocaleTimeString();
const logger = winston.createLogger({
  transports: [
    // colorize the output to the console
    new (winston.transports.Console)({
      timestamp: tsFormat,
      colorize: true,
    })
  ]
});

winston.level = 'debug';

if(config.token == ""){
    process.exit(1)
}

let controller = Botkit.slackbot({
  debug: false
})

var bot = controller.spawn({
    token: config.token,
    retry: Infinity
}).startRTM()


var downloadFromSubscence = function(movie,lan,msg){    
    console.log(msg)
    subscene_scraper.passiveDownloader(movie,lan,path)
        .then(function(savedFiles){
            console.log('subtitle saved to ',savedFiles);  
              bot.api.files.upload({file:fs.createReadStream(savedFiles[0]), channels: msg.channel},function(err,res) {
                    if (err) {
                        bot.reply("There is an error: ",err);
                        logger.error(err)
                    }

                });
            })
        .catch(function(err){
            bot.reply(msg, "There is an error: "+ err)
            logger.error("Substream: " + err);
            });}

var downloadFromAddic = function(movie,lang,msg,seas,epis,fullmovie){    
	console.log(movie,lang,seas,epis)
    addic7edApi.search(movie, seas, epis, lang).then(function (subtitlesList) {
    var subInfo = subtitlesList[0];
	console.log(subInfo)
	var subpath = movie + 'S' + seas + 'E' + epis + '-' + lang + '.srt';
	if(fullmovie !== ""){
		subpath = fullmovie + '-' + lang + '.srt'
	}
    if (subInfo) {
		bot.reply(msg,subInfo)
        addic7edApi.download(subInfo,subpath)
        .then(function () {
                console.log('Subtitles file saved.');
                bot.api.files.upload({file:fs.createReadStream(__dirname + '/' + subpath), channels: msg.channel},function(err,res) {
                if (err) {
                     bot.reply("There is an error: ",err);
                     logger.error(err)
                    }
                });

        });
    }
	else{
		bot.reply(msg,"There was an error downloading the subtitles. Subtitles couldn't be found or something else")
	}
    });
}


controller.on('bot_channel_join', function (bot, message) {
  bot.reply(message,
    "Hello world!"
  )
})

controller.hears('hello', 'direct_message,direct_mention,mention', function (bot,message) {
  bot.reply(message, 'Hey there! How can I help you?')
})

controller.hears(['info','about'], 'direct_message,direct_mention,mention', function (bot,message) {
  bot.reply(message, "Hey there! I'm a bot made by @matejmecka here to help you. You can download subtitles from me and get the link for the tv subtitles.")
})

controller.hears('subtitles subscene (.*) language (.*)', 'direct_message,direct_mention,mention', function (bot,message) {
    var todownload = message.match[1]
    var language = message.match[2]
	console.log("Subtitles Language")
    downloadFromSubscence(todownload,language,message)
});

controller.hears('subtitles subscene (.*)', 'direct_message,direct_mention,mention', function (bot,message) {
    var todownload = message.match[1]
    if(todownload == "help"){
            bot.reply(message,"To use the subtitles command you have to specify the file or name of the show/movie to download and language. If language is left empty the default value is going to be english.\n`subtitles [INSERTNAMEOFSHOWMOVIEHERE] language [INSERTLANGUAGEHERE]`\nExample: `subtitles me and earl and the dying girl language english`")
    }
    else{
        var language = "english"
        var file = downloadFromSubscence(todownload,language,message)
		console.log("subtitles")
    }
});

controller.hears('subtitles addic7ed (.*) season (.*) episode (.*) language (.*)', 'direct_message,direct_mention,mention', function (bot,message) {
    var todownload = message.match[1]
    var episode = message.match[3]
    var season = message.match[2]
    var language = message.match[4]
    logger.info('Addic7ed')
    downloadFromAddic(todownload,language,message,season,episode)

});

controller.hears('subtitles addic7ed (.*) season (.*) episode (.*)', 'direct_message,direct_mention,mention', function (bot,message) {
    var todownload = message.match[1]
    var episode = message.match[3]
    var season = message.match[2]
    var language = "eng"
    logger.info('Addic7ed')
    downloadFromAddic(todownload,language,message,season,episodeNumber)

});

controller.hears('subtitles addic7ed (.*) language (.*)', 'direct_message,direct_mention,mention', function (bot,message) {
	 var todownload = message.match[1]
	 var language = message.match[2]
		guessit.parseName(todownload).then(function (data) {
			downloadFromAddic(data.series,language,message,data.season,data.episode,todownload)
		});
});

controller.hears('subtitles addic7ed (.*)', 'direct_message,direct_mention,mention', function (bot,message) {
     var todownload = message.match[1]
     var language = "english"
        guessit.parseName(todownload).then(function (data) {
            downloadFromAddic(data.series,language,message,data.season,data.episodeNumber,todownload)
        });
});

controller.hears('subtitles --help', 'direct_message,direct_mention,mention', function (bot,message) {
     bot.reply(message,"To use the subtitles command you have to specify the file or name of the show/movie to download and language.\n If language is left empty the default value is going to be english.\nFor downloading using subscene use `subtitles subscene <nameoffilm> language <english>`.\n For downloading using addic7ed use `subtitles addic7ed <show> season <3> episode <6> language <eng>`")
});
controller.hears(['tvsamples','tv samples','tv-samples'], 'direct_message,direct_mention,mention', function (bot,message) {
  bot.reply(message, "https://ccextractor.org/public:general:tvsamples")
})

controller.hears('issues', 'direct_message,direct_mention,mention', function (bot,message) {
  bot.reply(message, "https://github.com/CCExtractor/ccextractor/issues")
})

controller.hears('help', 'direct_message,direct_mention,mention', function (bot,message) {
  bot.reply(message, "tvsamples -> Get the link to TV samples\n issues -> Get the link to the issue page\n subtitles -> Get Subtitles by specifying the name of a show or giving a file. `subtitles <subscene|addic7ed> <name> language <lang>`\n Example: `subtitles Interstellar language english`.\n Please use subtitles --help for more information.")
})


// Roses are Red, Violets are blue. Life is Meaningless, Watch Rick and Morty and eat Schezuan sauce.

